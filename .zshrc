#---------------------#
#  _________  _   _ 
# |__  / ___|| | | |
#   / /\___ \| |_| |
#  / /_ ___) |  _  |
# /____|____/|_| |_| 
#---------------------#

#-Exports-#
export WINEDEBUG=-all
export LC_CTYPE="en_US.UTF-8"
export LC_ALL="en_US.UTF-8"
export LANG="en_US.UTF-8"
export PATH="/home/avn/scripts:$PATH"
export PATH="/home/avn/.cargo/bin:$PATH"
export PATH="/home/avn/.local/bin:$PATH"
export PATH="/home/avn/.dotnet/tools:$PATH"
export PF_INFO="ascii title os shell editor wm pkgs memory"
export MESA_GL_VERSION_OVERRIDE=4.5
export WINEPREFIX=$HOME/.local/share/wineprefixes/hollow
export WINEARCH=win32 
export EDITOR=nvim
export HISTTIMEFORMAT="[%F %T]" 
export VISUAL=nvim
export WINEPATH=$HOME/.myapp
#--#

#-TTY AUTOSTART-#
clear
ttych
#--#


##----------------------------------------------------##
setopt autocd 
autoload -U colors && colors
##----------------------------------------------------##


##---------------------------------------------------------##
zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate
zstyle ':completion:*' group-name ''
zstyle :compinstall filename '/home/avn/.zshrc'
##---------------------------------------------------------##
#(cat ~/.cache/wal/sequences &)
##------------Autocomplete stuff----------------------------##
autoload -Uz compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)
export KEYTIMEOUT=1
setopt INC_APPEND_HISTORY
setopt HIST_IGNORE_ALL_DUPS
##----------------------------------------##


##---------------------------##
HISTFILE=~/.config/zsh/histfile
HISTSIZE=10000
SAVEHIST=1000
##---------------------------##


#-Aliases-#
##--------------------------------------------##
alias v="nvim"
alias dr="dotnet run"
alias c="clear && pfetch"
alias sv="sudo nvim"
alias zrc="nvim ~/.zshrc"
alias cfb="nvim ~/.config/bspwm/bspwmrc"
alias cfs="nvim ~/.config/sxhkd/sxhkdrc"
alias cfx="nvim ~/.xinitrc"
alias csp="cd ~/Pictures/Unsplash"
alias ss="cd ~/scripts"
alias ll="exa --header --long --git --all -F"
alias l="exa --long"  
alias grep="grep --color=auto"
alias ssi="systemctl suspend -i"
alias xrb="xrandr --output eDP-1 --brightness" 
alias q=exit 
alias mkd="mkdir -p -v" 
alias s="exa --long ~/scripts" 
alias cfa="nvim ~/.config/alacritty/alacritty.yml" 
alias pac="sudo pacman"
alias gs="git status"
alias cfp="nvim ~/.config/polybar/config.ini"
alias cfpi="nvim ~/.config/picom/picom.conf"
alias cp="cp -iv"
alias mv="mv -iv"
alias cfk="nvim ~/.config/kitty/kitty.conf"
alias cmatrix="unimatrix -s 97"
alias yay=paru
alias cfd="nvim ~/.config/dunst/dunstrc"
# alias ls="ls --color=auto"
alias msv="cd ~/Media/Videos/Songs"
alias cc="clear"
alias scrf="maim ~/Media/Pictures/shots/$(date +%s).png"
alias scrs="maim -s ~/Media/Pictures/shots/$(date +%s).png"
alias sntp="sudo ntpdate ntp.ubuntu.com"
##--------------------------------------------##


#-----#
#neofetch --source ~/Src/neofetch_ascii/skull.txt
pfetch
#-----#


##-------------------------------------------------------------------##
#eval "$(starship init zsh)"
PS1="%F{yellow}[%~%\] "
##-------------------------------------------------------------------##
#source /home/avn/Src/zsh-autosuggestions/zsh-autosuggestions.zsh
source /home/avn/Src/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
##-------------------------------------------------------------------##
